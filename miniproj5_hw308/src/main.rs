use serde::Deserialize;
use serde_json::{json, Value};
use simple_logger::SimpleLogger;
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use lambda_runtime::{Error as LambdaError, LambdaEvent, service_fn};
use std::collections::HashMap;

// Define the request structure
#[derive(Deserialize)]
struct Request {
    id: String,
    vehicle: String,
    size: String,
    price: String,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    SimpleLogger::new().with_utc_timestamps().init().unwrap_or_default();
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    // Deserialize the incoming Lambda event payload into the Request struct
    let request: Request = serde_json::from_value(event.payload)?;

    // Load AWS configuration and create a DynamoDB client
    let config = load_from_env().await;
    let client = Client::new(&config);

    // Attempt to post the new information to DynamoDB
    post_new_info(&client, &request.id, &request.vehicle, &request.size, &request.price).await?;

    // Return a success response
    Ok(json!({ "message": "Information successfully posted to DynamoDB" }))
}

async fn post_new_info(client: &Client, id: &str, vehicle: &str, size:&str, price: &str) -> Result<(), LambdaError> {
    let table_name = "vehicle"; // Ensure this matches your DynamoDB table name

    // Prepare the items to be inserted into the DynamoDB table
    let mut item = HashMap::new();
    item.insert("id".to_string(), AttributeValue::S(id.to_string()));
    item.insert("vehicle".to_string(), AttributeValue::S(vehicle.to_string()));
    item.insert("size".to_string(), AttributeValue::S(size.to_string()));
    item.insert("price".to_string(), AttributeValue::S(price.to_string()));

    // Send the put item request to DynamoDB
    client.put_item()
        .table_name(table_name)
        .set_item(Some(item))
        .send()
        .await.map_err(|err| {
            eprintln!("Error posting to DynamoDB: {}", err);
            LambdaError::from(err.to_string())
        })?;

    Ok(())
}
