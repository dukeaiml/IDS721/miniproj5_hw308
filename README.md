# miniproj5_hw308
This project created a Rust AWS Lambda function with a simple service, and connect to a database.

The lambda function with API gateway achieved to let user post vehicle information to a table `vehicle` of the database DynamoDB.

## Procedure
- Create a new [cargo](https://doc.rust-lang.org/cargo/getting-started/first-steps.html) project and add rust code to achieve the functionality of posting car information in the files `main.rs` and `Cargo.toml` for correct dependencies.  

- Create a new IAM role 'miniproj5' with granting access of `AmazonDynamoDBFullAccess`, `AWSLambda_FullAccess`, and `AWSLambdaBasicExecutionRole`. Then create a new table `vehicle` in DynamoDB service of AWS.

- Make sure that the table name in the `main.rs` should be align with the name created in DynamoDB.

- Build the application using `cargo build --release` and deploy it on AWS using the command `cargo lambda deploy --region --iam-role`.

- You may see the deployed lambda function on AWS and test its functionality for the action of database posting. 


## Screenshots

##### 1. New IAM Role with the correct permission policies.

![](images/IAM_role.png)

##### 2. Lambda function with API Gateway

![](images/lambda.png)


##### 3. Test for posting vehicle information sucessfully

![](images/test_sucess.png)
![](images/test.png)


##### 4. Log Record

![](images/log.png)


##### 5. Updated records in the table 'vehicle' in database DynamoDB 

![](images/database.png)
